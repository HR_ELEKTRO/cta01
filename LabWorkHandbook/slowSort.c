#include <stdio.h>

void slowSort(long long int a[], size_t first, size_t last);

void swap(long long int *p1, long long int *p2)
{
    long long int t = *p1;
    *p1 = *p2;
    *p2 = t;
}

// This function must be implemented in LEGv8 assembly
void slowSort(long long int a[], size_t first, size_t last)
{
    if (first != last)
    {
        size_t middle = (first + last) >> 1;
        slowSort(a, first, middle);
        slowSort(a, middle + 1, last);
        if (a[last] < a[middle])
        {
            swap(&a[last], &a[middle]);
        }
        slowSort(a, first, last - 1);
    }
}

int main()
{
    long long int a[] = {1, -2, 7, -4, 5};
    long long int b[] = {-4, -2, 1, 5, 7};

    slowSort(a, 0, sizeof(a)/sizeof(a[0]) - 1);
    size_t i;
    for (i = 0; i < sizeof(a)/sizeof(a[0]); i++)
    {
        if (a[i] != b[i])
        {
            printf("Error\n");
            return 0;
        }
    }
    printf("OK\n");
    return 0;
}
