#include <stdio.h>

unsigned long long int power(unsigned long long int n, unsigned long long int m);

// This function must be implemented in LEGv8 assembly
unsigned long long int power(unsigned long long int n, unsigned long long int m)
{
    if (m == 0) return 1;

    unsigned long long int p = 1;
    
    while (m != 1)
    {
        if ((m & 1) == 1) /* m is odd */
        {
            p = p * n;
        }
        n = n * n;
        m = m >> 1;
    }
    
    return p * n;
}

int main()
{
    unsigned int long long a = 7;
    unsigned int long long b = 21;

    if (power(a, b) == 558545864083284007)
    {
        printf("OK\n");
    }
    else
    {
        printf("Error\n");
    }
    return 0;
}
