#include <stdio.h>

void bubbleSortOpt(long long int a[], size_t n);

void swap(long long int *p1, long long int *p2)
{
    long long int t = *p1;
    *p1 = *p2;
    *p2 = t;
}

// This function must be implemented in LEGv8 assembly
void bubbleSortOpt(long long int a[], size_t n)
{
    size_t newn;
    do
    {
        newn = 0;
        size_t i;
        for (i = 1; i != n; i++)
        {
            if (a[i-1] > a[i])
            {
                swap(&a[i-1], &a[i]);
                newn = i;
            }
        }
        n = newn;
    }
    while (n != 0);
}

int main()
{
    long long int a[] = {1, -2, 7, -4, 5};
    long long int b[] = {-4, -2, 1, 5, 7};

    bubbleSortOpt(a, sizeof(a)/sizeof(a[0]));
    size_t i;
    for (i = 0; i != sizeof(a)/sizeof(a[0]); i++)
    {
        if (a[i] != b[i])
        {
            printf("Error\n");
            return 0;
        }
    }
    printf("OK\n");
    return 0;
}
