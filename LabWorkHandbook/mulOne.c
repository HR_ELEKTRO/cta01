#include <stdio.h>

unsigned long long int multiply(unsigned long long int a, unsigned long long int b);

/**
 * Multiplies two unsigned int values
 * @param unsigned long long int a
	The value of the multiplier a (0-18446744073709551615)
 * @param unsigned long long int b
	The value of the multiplicand b (0-18446744073709551615)
 * @return unsigned long long int
	The answer of a times b given that a * b < 18446744073709551616
 */

// This function must be implemented in LEGv8 assembly
unsigned long long int multiply(unsigned long long int a, unsigned long long int b)
{
	unsigned long long int i;
	unsigned long long int m = 0;

	for (i = 0; i != a; i++)
	{
		m = m + b;
	}

	return m;
}

int main()
{
    unsigned long long int a[] = {0, 1, 0, 1, 2000,          2,          1000000,                     1,            4294967295};
    unsigned long long int b[] = {0, 0, 1, 1,    2, 4294967295,       4294967295, 18446744073709551615u,            4294967296};
    unsigned long long int r[] = {0, 0, 0, 1, 4000, 8589934590, 4294967295000000, 18446744073709551615u, 18446744069414584320u};

    size_t i;
    for (i = 0; i != sizeof(a)/sizeof(a[0]); i++)
    {
    	printf("%llu x %llu: ", a[i], b[i]);
    	unsigned long long int result = multiply(a[i], b[i]);
    	unsigned long long int correct = r[i];
    	if (result != correct)
    	{
    		printf("Failed, function returned %llu but the correct answer is %llu\n", result, correct);
    	}
    	else
    	{
    		printf("Passed, %llu\n", result);
    	}
    }
    return 0;
}
