#include <stdio.h>

unsigned long long int sqrtFloor(unsigned long long int n);

// This function must be implemented in LEGv8 assembly
unsigned long long int sqrtFloor(unsigned long long int n)
{
    unsigned long long int p = 1u << 31;
    unsigned long long int r = 0;

    do
    {
        r = p | r;
		if (r * r > n)
        {
            r = r & ~p;
        }
        p = p >> 1;
	}
    while (p != 0);

	return r;
}

int main()
{
    unsigned long long int a[] = {0, 1, 2, 3, 4, 15241603688472100u, 15241603688472099u, 18446744073709551615u};
    unsigned long long int r[] = {0, 1, 1, 1, 2,         123456890,          123456889,            4294967295};

    size_t i;
    for (i = 0; i < sizeof(a)/sizeof(a[0]); i++)
    {
    	printf("sqrtFloor(%llu): ", a[i]);
    	unsigned long long int result = sqrtFloor(a[i]);
    	unsigned long long int correct = r[i];
    	if (result != correct)
    	{
    		printf("Failed, function returned %llu but the correct answer is %llu\n", result, correct);
    	}
    	else
    	{
    		printf("Passed, %llu\n", result);
    	}
    }
    return 0;
}
