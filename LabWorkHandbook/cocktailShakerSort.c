#include <stdio.h>

void cocktailShakerSort(long long int a[], size_t n);

void swap(long long int *p1, long long int *p2)
{
    long long int t = *p1;
    *p1 = *p2;
    *p2 = t;
}

// This function must be implemented in LEGv8 assembly
void cocktailShakerSort(long long int a[], size_t n)
{
    size_t j ;
    for (j = 0; j != n/2; j++)
    {
        size_t i;
        for (i = j; i != n - 1 - j; i++)
        {
            if (a[i] > a[i+1])
            {
                swap(&a[i], &a[i+1]);
            }
        }
        for (i = n - 2 - j; i != j; i--)
        {
            if (a[i-1] > a[i])
            {
                swap(&a[i-1], &a[i]);
            }
        }
    }
}

int main()
{
    long long int a[] = {1, -2, 7, -4, 5};
    long long int b[] = {-4, -2, 1, 5, 7};

    cocktailShakerSort(a, sizeof(a)/sizeof(a[0]));
    size_t i;
    for (i = 0; i != sizeof(a)/sizeof(a[0]); i++)
    {
        if (a[i] != b[i])
        {
            printf("Error\n");
            return 0;
        }
    }
    printf("OK\n");
    return 0;
}
