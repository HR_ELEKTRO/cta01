#include <stdio.h>

unsigned long long int dotProduct(unsigned long long int a[], unsigned long long int b[], size_t n);

// This function must be implemented in LEGv8 assembly
unsigned long long int dotProduct(unsigned long long int a[], unsigned long long int b[], size_t n)
{
    size_t i;
    unsigned long long int p = 0;
	for (i = 0; i != n; i++)
	{
		p = p + a[i] * b[i];
	}
	return p;
}

int main()
{
    unsigned long long int a[] = {1, 2, 3, 4, 5};
    unsigned long long int b[] = {10, 11, 12, 13, 14};

    if (dotProduct(a, b, 5) == 190)
    {
        printf("OK\n");
    }
    else
    {
        printf("Error\n");
    }
    return 0;
}
