#include <stdio.h>

void insertionSort(long long int a[], size_t n);

void swap(long long int *p1, long long int *p2)
{
    long long int t = *p1;
    *p1 = *p2;
    *p2 = t;
}

// This function must be implemented in LEGv8 assembly
void insertionSort(long long int a[], size_t n)
{
    size_t i;
	for (i = 0; i != n; i++)
	{
		size_t j;
        for (j = i; j != 0 && a[j-1] > a[j]; j--)
        {
            swap(&a[j], &a[j-1]);
        }
	}
}

int main()
{
    long long int a[] = {1, -2, 7, -4, 5};
    long long int b[] = {-4, -2, 1, 5, 7};

    insertionSort(a, sizeof(a)/sizeof(a[0]));
    size_t i;
    for (i = 0; i != sizeof(a)/sizeof(a[0]); i++)
    {
        if (a[i] != b[i])
        {
            printf("Error\n");
            return 0;
        }
    }
    printf("OK\n");
    return 0;
}
