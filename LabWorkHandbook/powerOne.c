#include <stdio.h>

unsigned long long int power(unsigned long long int n, unsigned long long int m);

// This function must be implemented in LEGv8 assembly
unsigned long long int power(unsigned long long int n, unsigned long long int m)
{
    unsigned long long int i;
    unsigned long long int p = 1;

	for (i = 0; i != m; i++)
	{
		p = p * n;
	}

	return p;
}

int main()
{
    unsigned int long long a = 7;
    unsigned int long long b = 21;

    if (power(a, b) == 558545864083284007)
    {
        printf("OK\n");
    }
    else
    {
        printf("Error\n");
    }
    return 0;
}
