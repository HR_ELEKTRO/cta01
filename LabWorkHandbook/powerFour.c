#include <stdio.h>

unsigned long long int power2(unsigned long long int p, unsigned long long int n, unsigned long long int m);

// This function must be implemented in LEGv8 assembly
unsigned long long int power2(unsigned long long int p, unsigned long long int n, unsigned long long int m)
{
    if (m == 0) return p;
    if (m == 1) return p * n;
    if ((m & 1) == 0) /* m is even */ return power2(p, n * n,  m >> 1);
    else /* m is odd */ return power2(p * n, n * n, m >> 1);
}

unsigned long long int power(unsigned long long int n, unsigned long long int m)
{
	return power2(1, n, m);
}

int main()
{
    unsigned int long long a = 7;
    unsigned int long long b = 21;

    if (power(a, b) == 558545864083284007)
    {
        printf("OK\n");
    }
    else
    {
        printf("Error\n");
    }
    return 0;
}
