# CTA01 - Computertechniek en -architectuur #

Dit repository is bedoeld voor studenten en docenten van de opleiding Elektrotechniek van de Hogeschool Rotterdam en wordt gebruikt om aanvullend studiemateriaal voor de module 'CTA01 - Computertechniek en -architectuur' te ontwikkelen en te verspreiden. 

**Deze module wordt niet meer aangeboden en is voor het laatst in het studiejaar 2021-2022 gegeven.**

Alle informatie is te vinden op de [Wiki](https://bitbucket.org/HR_ELEKTRO/cta01/wiki/).